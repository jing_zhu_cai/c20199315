#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void menu(); 
void help();
void one();void operation_1(); 
void two();void operation_2();
void three();void operation_3(); 
void error();
int main()
{
int opt=1,n;
printf("==========口算生成器==========\n");
printf("欢迎使用口算生成器:\n");
printf("\n");
help();
while(opt!=0)
{
menu();
printf("请输入操作> ");
scanf("%d",&opt);
printf("<执行操作:)\n");
printf("\n");
switch(opt)
{
case 1:operation_1();break;
case 2:operation_2();break;
case 3:operation_3();break;
case 4:help();break;
case 5:printf("程序结束, 欢迎下次使用\n");
printf("任意键结束……");
opt=0;
default:error();break;
}
}
return 0;
}
void menu()
{
printf("\n");
printf("操作列表:\n");
printf("1)一年级 2)二年级 3)三年级\n");
printf("4)帮助 5)退出程序\n");
}
void help()
{
printf("\n");
printf("帮助信息\n");
printf("您需要输入命令代号来进行操作,且\n");
printf("一年级题目为不超过十位的加减法\n");
printf("二年级题目为不超过百位的乘除法\n");
printf("三年级题目为不超过百位的加减乘除混合题目.\n");
}
void operation_1() 
{
printf("请输入生成个数>");
one();
}
void operation_2()
{
printf("请输入生成个数>");
two();
}
void operation_3()
{
printf("请输入生成个数>");
three();
}
void one()
{
int n,a,b,c;
scanf("%d",&n);
printf("一年级题目如下：\n"); 
srand((unsigned)time(NULL));
for(int i=1;i<=n;i++)
{
    a=rand()%10;
    b=rand()%10;
    c=rand()%2;
    if(c==0)
    printf("%2d + %2d = ___",a,b);
    else
    printf("%2d - %2d = ___",a,b);
    printf("\n");
 } 

}
void two()
{
int n,a,b,c;
scanf("%d",&n);
printf("二年级题目如下：\n"); 
srand((unsigned)time(NULL));
for(int i=1;i<=n;i++)
{
    a=rand()%100;
    b=rand()%100;
    c=rand()%2;
    if(c==0)
    printf("%3d * %3d = ___",a,b);
    else
    printf("%3d / %3d = ___",a,b);
    printf("\n");
 } 

}
void three()
{
int n,a,b,c;
scanf("%d",&n); 
printf("三年级题目如下：\n"); 
srand((unsigned)time(NULL));
for(int i=1;i<=n;i++)
{
    a=rand()%100;
    b=rand()%100;
    c=rand()%4;
    switch(c)
    {
        case 0:printf("%3d + %3d = ___",a,b);break;
        case 1:printf("%3d - %3d = ___",a,b);break;
        case 2:printf("%3d * %3d = ___",a,b);break;
        case 3:printf("%3d / %3d = ___",a,b);break;
     }
    printf("\n");
 }
}
void error()
{
printf("Error!!!\n");
printf("错误操作指令, 请重新输入\n");
}

